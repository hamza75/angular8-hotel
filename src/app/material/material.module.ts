import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatBadgeModule, MatButtonModule } from '@angular/material';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatBadgeModule,
    MatButtonModule
  ],
  exports: [
    MatBadgeModule, 
    MatButtonModule
  ]
})
export class MaterialModule { }
